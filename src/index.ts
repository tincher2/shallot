import ShallotAWS from './aws';
import ShallotAzure from './azure';

export { ShallotAWS, ShallotAzure };
